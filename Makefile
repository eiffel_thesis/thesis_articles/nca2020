CC=pdflatex
BIB=bibtex
SPELLCHECK=aspell

PAPER=nca.tex

TEX=$(wildcard TEX/*.tex)
FIGS_SVG=$(wildcard figs/SVG/*.svg)
FIGS_PDF=$(subst SVG,PDF,$(FIGS_SVG:.svg=.pdf))

# Get first PDF figure of FIGS_PDF with firstword then get its dirname with dir.
FIGS_PDF_DIR=$(dir $(firstword $(FIGS_PDF)))

all: $(FIGS_PDF)
	$(CC) $(PAPER)

draft: $(FIGS_PDF)
	$(CC) --jobname draft "\def\draft{1} \input{$(PAPER)}"

# For each figure in SVG convert it to PDF.
figs/PDF/%.pdf: figs/SVG/%.svg
	@if [ ! -d $(FIGS_PDF_DIR) ]; then mkdir $(FIGS_PDF_DIR); fi
	inkscape $< --export-area-drawing --without-gui --export-pdf=$@

bib: $(PAPER:.tex=.aux)
	$(BIB) $^

check: $(PAPER) $(TEX)
	# '-d en' tells to use english dictionnary, '-t' is to use LaTeX mode
	# and '-c' is to check.
	for i in $^; do $(SPELLCHECK) -d en -t -c $$i; done

clean:
	rm *.pdf *.aux *.bbl *.blg *.log *.out

mr_proper: clean
	rm -r figs/PDF
