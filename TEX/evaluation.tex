\section{Evaluation}
\label{sec_evaluation}
\subsection{Description of the experiment}
\label{subsec_description}
The goal of our experiment is to show that MemOpLight ensures both memory performance isolation and memory consolidation as opposed to existing Linux kernel mechanisms \cite{thesis_exps}.

\subsubsection{Metrics and baseline}
\label{subsubsec_baseline_description}
Our experiment studies three metrics: throughput, memory footprint and disk I/O.
We define throughput as the number of transactions that the benchmark is able to process in one second.
A transaction is a set of SQL requests that read from a database.
Memory footprint is the amount of memory used by the container during a given one-second period.
Disk I/O counts the number of reads done by the container during a second.
Throughput is measured from the benchmark output; memory size and disk activity are gathered by \texttt{docker}.
To compare fairly, we need reference numbers for these metrics.
These numbers were collected by running a reference experiment with a single container that executes the scenario of container A as described in Table \ref{table_two_load}.

\subsubsection{Two-container experiments}
\label{subsec_subsec_scenario}
Our experiments feature two concurrent containers, A and B, executing an OLTP workload.
Both experience changes in activity.
To test satisfaction, we arbitrarily set the SLO of container A to processing 1700 t/s and B 1100 t/s.
By doing so, we simulate the fact that A bought a better cloud offer than B.
The SLO are pictured as colored dashed lines in figures \ref{fig_transactions} to \ref{fig_transactions_mechanism} \cite{beyer_site_2016}.
When both containers are offered high load, we expect to observe memory performance isolation.
When one of the containers receives high load, and the other receives low load or is stopped, we expect memory consolidation to let the former reach the maximum throughput.
Accordingly, the experiment goes through six phases, each lasting 180 seconds, as described in Table \ref{table_two_load}.

\begin{table}
	\centering

	\caption{Summary of memory consolidation and memory performance isolation of existing mechanisms in Linux}
	\label{table_two_load}

	\begin{tabular}{c|c|c}
		\hline
		Phases & Container A & Container B\\
		\hline
		$\varphi_1(h, h)$\label{step_1} & high load & high load\\
		$\varphi_2(h, l)$\label{step_2} & high load & low load\\
		$\varphi_3(h, i)$\label{step_3} & high load & intermediate load\\
		$\varphi_4(l, l)$\label{step_4} & low load & low load\\
		$\varphi_5(i, h)$\label{step_5} & intermediate load & high load\\
		$\varphi_6(s, h)$\label{step_6} & stopped & high load\\
		\hline
	\end{tabular}
\end{table}

We use the notation: \step{n}{a}{b} where $n$ is the phase number, $a$ corresponds to the load of container A and $b$ that of container B.
Possible values for $a$ and $b$ are $h$ (high), $l$ (low), $i$ (intermediate) and $s$ (container is stopped).
During phase \step{1}{h}{h}, physical memory is smaller than the combined size of the databases, hence memory pressure occurs.
The size of the page cache decreases, causing an increase in physical disk I/O, hence a decrease in application throughput.
In \step{2}{h}{l}, if memory consolidation is effective, A should be able to take memory from B, and increase its throughput.
We expect both containers to have low throughput in \step{3}{h}{i}, similarly to \step{1}{h}{h}.
In \step{4}{l}{l}, containers should be able to answer all the transactions.
\step{5}{i}{h} mirrors \step{3}{h}{i}.
During \step{6}{s}{h}, as A stops completely, B should be able to reach maximum performance.

We run this scenario ten times and compute the mean and standard deviation of throughput, memory and disk I/O every second, plotted in Figure \ref{fig_default} to Figure \ref{fig_mechanism}.

\subsubsection{Experimental environment}
\label{subsubsec_problem_environment}
Our experimental machine is part of the Grid'5000 testbed \cite{grid5000, thesis_g5kscripts}.
It has two Intel\textregistered Xeon\textregistered Gold 6130 CPU clocked at \SI{2.1}{\giga\hertz}, with \SI{192}{\giga\byte} of DDR4 memory and an SSD \cite{intel_intel_2017}.
To create memory pressure, we artificially limit memory size, by running the experiments within a VM restricted to 4 CPU cores and \SI{3}{\giga\byte} of memory.

We use \texttt{qemu 3.1.0} as the VM hypervisor, \texttt{docker 19.03.2}  and its \texttt{python} library \texttt{docker-py 4.0.2} to manage containers \cite{qemu_qemu_2019, docker_docker_nodate, noauthor_docker_py_nodate, thesis_scripts}.
Our benchmark is \texttt{sysbench oltp} \cite{alexey_kopytov_sysbench_nodate}.
We modified the benchmark code in order to be able to vary the throughput \cite{thesis_sysbench}.
The benchmark reads requests from a \texttt{mysql 5.7} database \cite{noauthor_mysql_nodate, thesis_images}.
We run our experiments on Linux 4.19 \cite{greg_kroah_hartman_linux_2018}.

Our two containers (A and B) run with two cores each.
Each one reads a database of \SI{4}{\giga\byte}.

\subsection{Baseline experiment with one container}
\label{subsec_problem_reference}
After running the reference experiment, we measure the maximum throughput to be 2000 transactions per second (t/s).
The maximum memory footprint and maximum disk I/O are measured at \SI{2.8}{\giga\byte} and 100 reads per second respectively.
These values are pictured in Figures \ref{fig_default} to \ref{fig_mechanism} as dashed black horizontal lines.

We arbitrarily set ``low'' throughput to 10\% of high throughput, \textit{i.e.}, 200 t/s.
Low throughput is depicted as horizontal dotted black lines in Figure \ref{fig_transactions} to Figure \ref{fig_transactions_mechanism}.
We also define ``intermediate'' throughput as 1500 t/s.

\input{TEX/figure.tex}

\subsection{Conventionnal mechanisms with two containers}
\label{subsec_evaluation_two_containers_linux_mechanism}
Let us briefly interpret figures \ref{fig_default} to \ref{fig_soft}.
A more detailled analysis is available in previous work \cite{laniel_highlighting_2019}.

Without any limit set, \step{1}{h}{h} of Figure \ref{fig_transactions} shows that there is no isolation, since containers have the same throughput.
In this figure, in \step{2}{h}{l}, A increases its performance, because B receives low load, but it does not reach its reference level.
During \step{6}{s}{h}, as A stops, B is able to increase its memory footprint, as shown in Figure \ref{fig_memory}, and so reaches the reference performance.


Figure \ref{fig_transactions_max} depicts the situation when the \texttt{max} limit is set to \SI{1.8}{\giga\byte} for A and \SI{1}{\giga\byte} for B.
A has better performance than B in \step{1}{h}{h}.
This can be explained by looking at the same phase in Figure \ref{fig_memory_max}.
Indeed, container footprints follow their \texttt{max} limits, so A has more memory than B.
Note that, in \step{6}{s}{h}, where A is stopped, B cannot increase its performance because its footprint is blocked by its \texttt{max} limit.

The \texttt{soft} limit results are similar to that of \texttt{max} limit.
The only difference occurs in \step{6}{s}{h} of Figure \ref{fig_transactions_soft}, where B reaches reference performance.
Indeed, as A is stopped, there is no more memory pressure and B can increase its footprint as shown in Figure \ref{fig_memory_soft}.

Table \ref{table_summary}, presented earlier, summarizes these results.

\subsection{MemOpLight with two containers}
\label{subsec_evaluation_two_containers}
MemOpLight is designed to consolidate memory while ensuring memory performance isolation.
To verify this, we run again the experiment this time enabling MemOpLight.
We plot the results in Figure \ref{fig_mechanism}.

\step{1}{h}{h}, \step{3}{h}{i} and \step{5}{i}{h} in Figure \ref{fig_transactions_mechanism} follow the same pattern as the same phases in Figure \ref{fig_transactions_soft}.
Figure \ref{fig_memory_mechanism} shows that, like Figure \ref{fig_memory_soft}, when both containers receive high or intermediate load, their memory footprints follow their \texttt{soft} limits (\step{1}{h}{h}, \step{3}{h}{i} and \step{5}{i}{h}).
A has then a larger footprint than B, this shows that MemOpLight ensures memory performance isolation.
Moreover, in these phases, performance of containers A and B are better than with \texttt{max} and \texttt{soft} limits.
On average, container A answers to 1351, 1362 and 1370 transactions per second compared to 1290, 1314 and 1358 with the \texttt{max} limit; this represents an increase of respectively 4.7\%, 3.7\% and 0.9\%.
With MemOpLight, container B handles almost 933, 999 and 1036 transactions per second while it processes only 894, 968 and 978 transactions per second with the \texttt{max} limit; resulting in an increase of 4.4\%, 3.2\% and 5.9\%.
These increases can be explained because MemOpLight permits converging to a balance where containers stop stealing memory each other.

Like \step{6}{s}{h} in Figure \ref{fig_memory_soft}, when A stops completely, there is no more memory pressure, B's footprint grows and permits it to increase its performance to the maximum throughput.

MemOpLight's dynamic nature brings a real benefit in \step{2}{h}{l}.
During this phase, B receives low load and A a high one.
As shown in Figure \ref{fig_memory_mechanism}, there is a memory transfer from container B to container A.
Since container B has no requests in its queue, its memory is reclaimed until finding the threshold where it can still answer to 200 t/s.
The application feedback permits finding this memory configuration without previous static analysis.
MemOpLight maximizes memory consolidation so container A's throughput equals its SLO.

\begin{table*}
	\centering

	\caption{Median value of measured throughput, averaged over 10 runs, in each phase of each experiment (in t/s rounded to the nearest integer)}
	\label{table_sumup}

	\begin{tabular}{l|c|c|c|c|c|c|c|c|c|c|c}
		\hline
		\diagbox{Transactions}{Phases} & \multicolumn{2}{c|}{\step{1}{h}{h}} & \multicolumn{2}{c|}{\step{2}{h}{l}} & \multicolumn{2}{c|}{\step{3}{h}{i}} & \multicolumn{2}{c|}{\step{4}{l}{l}} & \multicolumn{2}{c|}{\step{5}{i}{h}} & \step{6}{s}{h}\\
		\hline
		Container & A & B & A & B & A & B & A & B & A & B & B\\
		\hline
		Input load & 2000 & 2000 & 2000 & 200 & 2000 & 1500 & 200 & 200 & 1500 & 2000 & 2000\\
		\hline
		Limits not set & 1053 & \textbf{1043} & 1374 & 196 & 1054 & \textbf{1168} & 195 & 197 & 1195 & \textbf{1145} & \textbf{1751}\\
		\texttt{Max} limit & 1290 & 894 & 1411 & 196 & 1314 & 968 & 196 & 660 & 1358 & 978 & 962\\
		\texttt{Soft} limit & 1268 & 879 & 1423 & 197 & 1299 & 969 & 196 & 794 & 1360 & 970 & 974\\
		MemOpLight & \textbf{1351} & 933 & \textbf{1670} & 195 & \textbf{1362} & 999 & 196 & 197 & \textbf{1370} & 1036 & 1723\\
	\end{tabular}
\end{table*}

Table \ref{table_sumup} summarizes all the previous measurements.
B has lower performance with MemOpLight and would have better performance with no limits set.
This behavior is normal, since in the latter case there is no memory performance isolation.
Nonetheless, this is not expected in cloud since a client could have paid more than another so one client would have a better offer than the other.

In summary, MemOpLight increases performance of containers by redistributing memory following their needs.

\subsection{MemOpLight with eight containers}
\label{subsec_evaluation_eight_containers}
To confirm that MemOpLight adapts to application loads thanks to application feedback, we now run an experiment with 8 containers.
Along the phases, which each lasts for 120 seconds, containers receive high, intermediate or low load.
The container loads were chosen randomly to cover different cases and put MemOpLight in difficulty.

The scenario and its characteristics are described in Table \ref{table_load}.
The first column is SLO.
The second one shows the values for \texttt{max} and \texttt{soft} limits.
The other columns describe the load at each phase of the experiment.
Throughput can have multiple values:
\begin{itemize}
	\item High: The container receives 2500 t/s.
	Black cells, in Table \ref{table_load}, depicts phases where containers receive high load.
	\item Intermediate : The container receives its SLO $\pm 5\%$ t/s.
	In Table \ref{table_load}, gray and light gray cells show phases with intermediate load.
	\item Low: The container sustains only 200 t/s.
\end{itemize}

The different phases of the scenario can be grouped to form 4 different periods.
In \stepeight{1} and \stepeight{2}, the system is highly loaded.
\stepeight{3} is a transition between highly and lowly loaded.
In \stepeight{4}, the containers receive low loads.
During \stepeight{5} to \stepeight{9}, the system is moderately busy but container loads increase over time.

\begin{table*}
	\centering

	\caption{Throughput in each phase of experiment (in transactions per second)}
	\label{table_load}

	\begin{tabular}{l|c||c||c|c|c|c|c|c|c|c|c}
		\hline
		Containers & SLO (t/s) & Limit (if set) & \stepeight{1} & \stepeight{2} & \stepeight{3} & \stepeight{4} & \stepeight{5} & \stepeight{6} & \stepeight{7} & \stepeight{8} & \stepeight{9}\\
		\hline
		A & 1800 & \SI{1400}{\mega\byte} & \cellcolor{black} \textcolor{white}{2500} & \cellcolor{blue_grey_200} 1710 & \cellcolor{black} \textcolor{white}{2500} & 200 & \cellcolor{blue_grey} 1890 & 200 & 200 & 200 & \cellcolor{black} \textcolor{white}{2500}\\
		B & 1600 & \SI{1000}{\mega\byte} & \cellcolor{blue_grey_200} 1520 & \cellcolor{black} \textcolor{white}{2500} & \cellcolor{black} \textcolor{white}{2500} & 200 & \cellcolor{black} \textcolor{white}{2500} & 200 & \cellcolor{blue_grey_200} 1520 & \cellcolor{black} \textcolor{white}{2500} & 200\\
		C & 1400 & \SI{800}{\mega\byte} & \cellcolor{blue_grey_200} 1330 & \cellcolor{blue_grey} 1470 & 200 & \cellcolor{blue_grey} 1470 & \cellcolor{black} \textcolor{white}{2500} & \cellcolor{blue_grey_200} 1330 & \cellcolor{blue_grey} 1470 & 200 & \cellcolor{blue_grey} 1470\\
		D & 1400 & \SI{800}{\mega\byte} & \cellcolor{black} \textcolor{white}{2500} & \cellcolor{blue_grey} 1470 & 200 & 200 & 200 & \cellcolor{blue_grey} 1470 & 200 & \cellcolor{black} \textcolor{white}{2500} & \cellcolor{black} \textcolor{white}{2500}\\
		E & 1200 & \SI{600}{\mega\byte} & \cellcolor{blue_grey_200} 1140 & \cellcolor{blue_grey_200} 1140 & 200 & \cellcolor{black} \textcolor{white}{2500} & \cellcolor{blue_grey} 1260 & \cellcolor{black} \textcolor{white}{2500} & \cellcolor{black} \textcolor{white}{2500} & \cellcolor{black} \textcolor{white}{2500} & 200\\
		F & 1200 & \SI{600}{\mega\byte} & \cellcolor{black} \textcolor{white}{2500} & \cellcolor{blue_grey} 1260 & 200 & \cellcolor{black} \textcolor{white}{2500} & 200 & \cellcolor{black} \textcolor{white}{2500} & \cellcolor{blue_grey} 1260 & \cellcolor{blue_grey} 1260 & \cellcolor{black} \textcolor{white}{2500}\\
		G & 1000 & \SI{400}{\mega\byte} & \cellcolor{black} \textcolor{white}{2500} & 200 & \cellcolor{black} \textcolor{white}{2500} & 200 & 200 & 200 & 200 & \cellcolor{blue_grey_200} 950 & \cellcolor{blue_grey_200} 950\\
		H & 800 & \SI{400}{\mega\byte} & \cellcolor{blue_grey} 840 & \cellcolor{black} \textcolor{white}{2500} & 200 & 200 & \cellcolor{blue_grey_200} 760 & 200 & \cellcolor{blue_grey_200} 760 & \cellcolor{blue_grey_200} 760 & \cellcolor{black} \textcolor{white}{2500}\\
	\end{tabular}
\end{table*}

In this experiment, we focus on the respect of SLO and the global performance.
Figure \ref{fig_eight_throughput} plots the average throughput of containers generated across 10 runs for different mechanisms.
The global throughput is depicted by the height of the bars while the different colors indicate the throughput of each container.
Hatched colors emphasize throughput that is above SLO.
Figure \ref{fig_eight_color} focuses on the respect of the SLO.
It plots the colors of containers over time during the sixth run of our experiment.
To plot this Figure, we activate the probes for all mechanisms.
As we discussed in Section \ref{subsec_probe}, the probe has no overhead.

\begin{figure}[h]
	\centering

	\subfloat[Limits not set]{
		\includegraphics[scale = .50]{default_stacked_transactions.pdf}

		\label{fig_eight_default}
	}

	\subfloat[\texttt{Max} limits]{
		\includegraphics[scale = .50]{max_stacked_transactions.pdf}

		\label{fig_eight_max}
	}

	\subfloat[\texttt{Soft} limits]{
		\includegraphics[scale = .50]{soft_stacked_transactions.pdf}

		\label{fig_eight_soft}
	}

	\subfloat[MemOpLight]{
		\includegraphics[scale = .50]{mechanism_stacked_transactions.pdf}

		\label{fig_eight_mechanism}
	}

	\caption{Average throughput of eight containers during each phase with different mechanisms}
	\label{fig_eight_throughput}
\end{figure}

\begin{figure}[h]
	\centering

	\subfloat[Limits not set]{
		\includegraphics[scale = .5]{default_color.pdf}

		\label{fig_eight_color_default}
	}

	\subfloat[\texttt{Max} limits]{
		\includegraphics[scale = .5]{max_limit_color.pdf}

		\label{fig_eight_color_max}
	}

	\subfloat[\texttt{Soft} limits]{
		\includegraphics[scale = .5]{soft_limit_color.pdf}

		\label{fig_eight_color_soft}
	}

	\subfloat[MemOpLight]{
		\includegraphics[scale = .5]{mechanism_color.pdf}

		\label{fig_eight_color_mechanism}
	}

	\caption{Containers' colors during the 5th run with different mechanisms}
	\label{fig_eight_color}
\end{figure}

In \stepeight{1} and \stepeight{2}, the system is overloaded since almost all containers receive high or intermediate loads.
The global performance is lower with limits not set, \textit{e.g.} 836453 transactions (t.) compared to 895317.8 t. and 898728 t. for \texttt{max} and \texttt{soft} limits, because containers are fighting for memory so it is used ineffectively.
For these phases, MemOpLight achieves the best global performance (927651 t. in \stepeight{1} and 937413 t. in \stepeight{2}).
Indeed, with MemOpLight, once containers reach a memory balance they stop fighting for memory.
Even containers which have less memory have better performance because they use it effectively.

\stepeight{3} is a transition phase between highly and lowly loaded.
This phase shows if mechanisms are able to adapt to this change.
During this phase, \texttt{max} and \texttt{soft} limits permit better performance than limits not set in \stepeight{3} (779570 t. and 762521 t. compared to 729834 t.).
As depicted in Figure \ref{fig_eight_mechanism}, MemOpLight permits a better global performance (788909), thanks to its dynamic behavior.
Figure \ref{fig_eight_color_mechanism} shows that MemOpLight permits to containers to converge faster to their SLO.

In \stepeight{4}, containers continue to receive low loads.
In this phase, the \texttt{max} and \texttt{soft} limits activate so they block memory consolidation and impede container performance (583156 t. and 580914 t. compared to 714737 t. without limits set).
MemOplight offers the same performance as with limits not set (723084 t. \textit{vs.} 714737 t.).
With MemOpLight, all containers are satisfied, since there are only green containers in \stepeight{4} in Figure \ref{fig_eight_color_mechanism}.
It also permits to more containers to exceed their SLO since hatched zones are bigger.

In \stepeight{5} to \stepeight{9}, the system is moderately busy but container loads increase over time.
Figure \ref{fig_eight_color_mechanism} shows that MemOpLight maximizes container satisfaction.
Moreover, at the beginning of each phase, we can see that containers reach satisfaction more quickly thanks to the dynamic application feedback.
During these phases, note that peaks of red can be explained because the benchmark offers an average load, not a constant one.

This experiment shows that MemOpLight permits consolidation and isolation as shown in Table \ref{table_sumup_eight} and Table \ref{table_sumup_eight_percent}.
Table \ref{table_sumup_eight} presents performance for each container with all the tested mechanisms.
One can see that MemOpLight supports better performance for five of the eight containers.
Where MemOpLight performs worse, the difference is only about 0.6\%, 2.3\% and 1.9\% for container B, G and H compared to the experiment with limits not set.
Overall, MemOpLight reaches 122.6 millions t. which is 8.9\% better than with limits not set.
Table \ref{table_sumup_eight_percent} shows fraction of time where containers are satisfied.
This time is the sum of yellow and green time.
MemOpLight maximizes the number of satisfied containers.
It also increases the time passed satisfied throughout the whole experiment from 44\% to 57\%.
Thus, MemOpLight enhances efficiency, since it improves performance with the same hardware.

\begin{table*}
	\centering

	\caption{Total containers' throughputs (in million of requests with different mechanisms, averaged over 10 runs)}
	\label{table_sumup_eight}

	\begin{tabular}{l|cccccccc|c}
		\hline
		\diagbox{Mechanism}{Containers} & A & B & C & D & E & F & G & H & Total\\
		\hline
		No limits set & 15.2 & 14.5 & 17.3 & 11.9 & 18.7 & 15.9 & \textbf{8.8} & \textbf{10.3} & 112.6\\
		\texttt{Max} limit & 13.6 & 13.8 & 13.8 & 8.5 & 14.9 & 9.9 & 4.6 & 6.8 & 85.9\\
		\texttt{Soft} limit & 17.1 & \textbf{17.4} & 17.6 & 12.6 & 17.5 & 13.4 & 7.3 & 9.5 & 112.4\\
		MemOpLight & \textbf{17.5} & 17.3 & \textbf{18.4} & \textbf{13.7} & \textbf{20.2} & \textbf{16.8} & 8.6 & 10.1 & \textbf{122.6}\\
	\end{tabular}
\end{table*}

\begin{table*}
	\centering

	\caption{Fraction of time (in percent) where container is satisfied (\textit{i.e.} yellow or green) according to mechanisms (averaged over 10 runs)}
	\label{table_sumup_eight_percent}

	\begin{tabular}{l|cccccccc|c}
		\hline
		\diagbox{Mechanism}{Containers} & A & B & C & D & E & F & G & H & Total\\
		\hline
		No limits set & 42 & 23 & 8 & 35 & 69 & 39 & 62 & \textbf{71} & 44\\
		\texttt{Max} limit & \textbf{60} & 35 & 5 & 37 & 73 & 15 & 53 & 35 & 39\\
		\texttt{Soft} limit & 56 & 42 & 11 & 35 & 64 & 16 & 53 & 32 & 39\\
		MemOpLight & 58 & \textbf{44} & \textbf{42} & \textbf{52} & \textbf{76} & \textbf{53} & \textbf{67} & 67 & \textbf{57}\\
	\end{tabular}
\end{table*}

In summary, MemOpLight improves throughput and enables containers to be more satisfied.

\subsection{Study of MemOpLight parameters}
\label{subsec_memoplight_parameters_study}
MemOplight has two parameters that can be tuned: the percent of memory reclaimed each period and the duration of a period.
In its implementation, we also chose to reclaim memory from yellow containers, this choice can be discussed.

In this subsection, we first study the impact of these parameters on MemOpLight's performance.
For that, we run again the experiment featuring two containers.
Then, we modify MemOpLight to create MemOpLightNoYellow where yellow containers are not reclaimed.
To compare them, we use again the experiment with eight containers.
Unfortunately and due to lack of space, we cannot display the associated figures.

First, we set the reclaim period to be \SI{1}{\second}, and the percent to the following values: 1\%, 2\%, 5\% and 10\%.
The results for this different values are quite the same.
This behavior is due to the using of the PFRA to reclaim memory.
This mechanism offers a function which has a memory size to reclaim as parameter.
But, this parameter is only indicative, the kernel does whatever it can.

We now fix the memory percent to 2\% and use this values for the reclaim period: \SI{1}{\second}, \SI{2}{\second}, \SI{5}{\second} and \SI{10}{\second}.
This time, the results are totally different and a reclaim period of \SI{10}{\second} shows bad performance.
Particularly, for \step{2}{h}{l}, a period of \SI{10}{\second} leads to a median throughput of 1456 t/s compared to the 1638 t/s offered by a period of \SI{1}{\second}.
This represents a decrease of 10.6\%.
So, the reclaim period has to conform with the application activity, if the application reacts quickly to event, the period has to be short, otherwise it can be longer.

To finish, we study the impact of not reclaiming memory from yellow containers by comparing MemOpLight and MemOpLightNoYellow.
MemOplight performs better than MemOpLightNoYellow, because containers exceed more their SLO in some phases.
More precisely, the averaged total transactions is 122.6 millions for MemOpLight and 118.9 for MemOpLightNoYellow.
The global satisfaction also drops from 57\% to 52\%.

If yellow containers are not reclaimed, there are fewer containers to reclaim, so red containers can less improve their performance.
Moreover, yellow containers could need a lot of memory to become green.
For these reasons, the yellow state is important and the performance results proved it.