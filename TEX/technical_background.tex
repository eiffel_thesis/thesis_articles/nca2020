\section{Technical background}
\label{sec_technical}
Containers are based on the Linux \texttt{cgroup} structure for grouping a number of processes \cite{hiroyu_cgroup_2008, rami_rosen_namespace_2016, linux_memory_nodate, zhenyun_zhuang_taming_2017}.
A \texttt{cgroup} limits the collective resource usage of its processes.
Particularly, the total amount of physical memory used across all the processes of a \texttt{cgroup} is capped by the \texttt{max} and \texttt{soft} limits, which we describe later.
In the rest of this section, we study how Linux manages the memory of containers under \emph{memory pressure}, \textit{i.e.}  when free physical memory is scarce.

\subsection{No limits}
\label{subsec_technical_default}
When memory pressure occurs, and if the \texttt{max} and the \texttt{soft} limits are not set, memory is reclaimed from all containers.
For instance, if the memory requirements of some container decreases, the kernel reallocates unused memory to another container.
This constitutes memory consolidation.
Unfortunately, as Linux allocates unused memory to the file cache, it does not enforce memory performance isolation \cite{the_kernel_development_community_concepts_nodate}.
Therefore, I/O of one container can impede another container's performance without increasing the performance of the former.
In summary, when no limits are set, memory consolidation occurs, at the expense of memory performance isolation.

\subsection{The \texttt{max} and \texttt{soft} limit mechanism}
\label{subsec_technical_max_soft}
To solve this problem, Linux provides the \texttt{max} and the \texttt{soft} limits to guide the memory reclamation of containers.
By default, these limits are unset; it is up to the user to set them through \texttt{sysfs}.
A container's memory footprint cannot exceed its \texttt{max} limit.
Even if there are free pages, the kernel will not allocate them to a container that has reached its \texttt{max} limit.
If a process needs physical memory, and its container has already reached its \texttt{max} limit, the kernel may reallocate memory from another process of the same container, but not from another container.
This mechanism ensures isolation, by avoiding that some container would starve the others.

The \texttt{soft} limit is similar, except that it is active only when there is memory pressure.
A container's allocation may occasionally exceed its \texttt{soft} limit if there is free physical memory.
To guarantee good performance under memory pressure, the administrator should set the \texttt{soft} limit to approximate the container's Working Set (WS) size \cite{denning_working_1967}.
Unfortunately, it is hard to estimate WS size \cite{gregg_working_2018, nitu_working_2018}.

To summarize, container memory footprint is limited by the \texttt{soft} limit when memory pressure occurs, and by its \texttt{max} limit at all times.
However, these settings are not dynamically related to the containers' current memory needs; they may be higher (impeding memory consolidation) or lower (impeding memory performance isolation).