\section{Introduction}
\label{sec_intro}
A cloud provider is allowed to \emph{consolidate} the logical servers of different clients on the same underlying machine, thus amortizing cost \cite{zhang_virtualization_2012}.
Each client has the responsibility to correctly size the virtual resources that they rent in order to ensure that they execute smoothly \cite{amazon_sla_nodate, microsoft_sla_nodate, ovh_conditions_nodate}.
However, there is a trade-off.
The execution of one logical server should not disturb the others: the logical servers should remain \emph{isolated} from one another.
In self-managed clouds, the goal is to maximize, on any single machine, the number of applications that respect their SLO (Service Level Objectives) with the minimal amount of hardware.

To ensure both consolidation and isolation, a common approach is to use Virtual Machines (VM).
Unfortunately, a VM is heavyweight and waste resources.
Resource transfer between VM is complex and difficult to automate \cite{kvm_automatic_2013, mammedov_featurescpuhotplug_2017}.
A recent alternative is the ``container'', a group of processes with sharing and isolation properties \cite{amazon_amazon_nodate, microsoft_microsoft_nodate, alibaba_alibaba_nodate, ovh_ovh_nodate}.
Containers support security (\textit{e.g.}, a file in one container cannot be accessed by another), ease of deployment (\textit{e.g.}, it is possible to start a container with a simple shell command) and fine-grain resource control (\textit{e.g.}, a container can be pinned to a specific CPU core) \cite{docker_inc_what_nodate}.

To ensure \emph{memory performance isolation}, \textit{i.e.}, guaranteeing to each container enough memory for it to perform well, the administrator limits the total amount of physical memory that the container's processes can use.
If it exceeds its limits, some of its memory will be reclaimed, making it available to others.
Notably, the Linux kernel reclaims pages from the file page cache, resulting in a performance decrease in containers that perform I/O \cite{the_kernel_development_community_concepts_nodate}.

In previous work, we showed that the memory consolidation provided by the limit mechanism is imperfect \cite{laniel_highlighting_2019}.
Moreover, the limits size are static, and do not adapt to the containers' dynamic behavior.
This is a problem, because it is hard to estimate, \textit{a priori}, the required amount of memory satisfying an application’s performance objectives \cite{gregg_working_2018, nitu_working_2018}.
Furthermore, the metrics available to the kernel to evaluate its policies (\textit{e.g.}, frequency of page faults, I/O requests, use of CPU cycles, \textit{etc.}) are not directly relevant to performance.
Indeed, the application performance is better characterized by application-level metrics, such as response time or throughput \cite{weiner_psi_2018}.

To solve these problems, we propose a new approach, called the Memory Optimization Light\footnote{Our solution uses the colors of traffic lights to indicate container performance.} (MemOpLight).
It is based on application-level feedback from containers.
Our mechanism aims to rebalance memory allocation in favor of unsatisfied containers, while not penalizing the satisfied ones.
As a side effect, this improves overall resource consumption while consolidating memory.
MemOpLight is intended to be used by cloud providers, to make better use of the underlying infrastructure, while guaranteeing good performance to client tasks.
The memory of containers receiving low load can be reclaimed, in order to improve performance of these receiving high ones.
% Avec deux conteneurs : 1362 / 1054 = 1.2922201138519924
Our experiments show that MemOpLight increases throughput up to 29.2\% compared to the default for the \texttt{sysbench} benchmark accessing a \texttt{mysql} database.

Our main contributions are the following:
\begin{inparaenum}[(i)]
	\item The design of a simple feedback mechanism, from application to kernel.
	\item An algorithm for adapting container memory allocation.
	\item An implementation in Linux and its experimental evaluation.
\end{inparaenum}
We organize the remainder of our paper as follows.
Section~\ref{sec_technical} presents some technical background.
Section~\ref{sec_problem} summarizes the issues with existing Linux mechanisms.
Section~\ref{sec_solution} presents MemOpLight.
Section~\ref{sec_evaluation} compares its performance with existing Linux mechanisms.
In Section~\ref{sec_related_works}, we review related work.
Finally, we conclude and discuss future work in Section~\ref{sec_conclusion}.