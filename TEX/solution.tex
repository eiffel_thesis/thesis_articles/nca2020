\section{Memory optimization light (MemOpLight)}
\label{sec_solution}
\subsection{MemOpLight's functioning}
It is difficult to do better with the static limits and the limited information available to the kernel.
The kernel only has access to low-level metrics such as the CPU cycles used or the I/O bandwidth.
Theses are, at best, proxies for application-level Quality of Service (QoS), and are not representative of the needs of modern applications.

Instead, we propose to base memory reclamation on performance metrics perceived by the application.
The container itself declares its current level of QoS, according to its own, application-specific metrics (\textit{e.g.} latency for a web server, frame rate for a streaming application, etc.), compared to some performance objectives.
We associate a traffic light color to different states of performance:
\begin{itemize}
	\item GREEN: The container has reached its maximum level and would not benefit from more resources.
	For instance, a web server has no requests waiting.
	\item YELLOW: The container is satisfied with its QoS, but it would do better with more resources.
	For example, a web server has pending requests in a queue but has answered the earlier with sufficiently low response time.
	\item RED: The container is not satisfied, \textit{e.g.} a web server is not able to answer requests under a certain latency.
\end{itemize}
Initially a container starts in the RED state.

Our approach is based on three components.

\paragraph{User-kernel communication}
We develop a communication mechanism between user and kernel, so a container can communicate its state of performance to the kernel.
The Linux kernel provides multiple mechanism for this purpose (\textit{e.g.}, syscall, ioctl,  sysfs, socket, \textit{etc.}).
We choose the \texttt{sysfs} because it is less intrusive for the kernel and has a file-oriented API which is easy to use by users.

\paragraph{Probe}
A container is equipped with an application probe, which indicates its color, based on some application-specific.
The probe could either exploit existing information in the application, or be a separate script that collects information about the application.
For \texttt{mysql}, we developed a script that reads the database log and analyzes the request latency.
In our prototype, the probe compares the transaction latency of \texttt{mysql}, which executes inside the container, to some SLO and informs Linux through \texttt{sysfs} every second.
Specifically, a container with high throughput declares itself green if its throughput equals the SLO and has no transactions waiting.
With low throughput, it is green if it handles $\approx 200$ t/s.
If it cannot respect its SLO, it is red.
Otherwise, if it has transactions waiting, it is yellow.

\paragraph{MemOpLight}
Our third component is an algorithm that executes when memory is scarce.
MemOpLight extends the Linux Page Frame Reclaim Algorithm (PFRA) \cite{bovet_understanding_2006}.
When a container needs memory, it takes it from the free physical pages.
The PFRA activates when a memory threshold is reached, \textit{i.e.} when free physical memory becomes scarce.
It aims to reclaim memory and bring the free physical memory above the threshold.
Regarding containers, the PFRA has two parts.
The first one consists in recycling memory from one container (local reclaim) while the other reclaims memory from all containers (global reclaim).
MemOpLight modifies the PFRA behavior and reclaims memory from containers based on their colors, there are multiple cases shown in Table \ref{table_reclaim_cases}.

\begin{table}
	\centering

	\caption{The different reclamation cases of MemOpLight}
	\label{table_reclaim_cases}

	\begin{tabular}{ccp{.2\textwidth}}
		\hline
		Red \texttt{cgroups} & Yellow \texttt{cgroups} & Result\\
		\hline
		$0$ & $0$ & No \texttt{cgroups} are reclaimed from.\\
		$0$ & $\ge 1$ & Green \texttt{cgroups} are reclaimed from.\\
		$\ge 1$ & $0$ & Green \texttt{cgroups} are reclaimed from.\\
		$\ge 1$ & $\ge 1$ & Green \texttt{cgroups} are reclaimed from first then yellow ones.\\
		\hline
	\end{tabular}
\end{table}

We make two design decisions.
The first is to reclaim from both green and yellow containers when there is a red one.
By doing so, we hope to maximize the number of satisfied containers quickly.
The other is to limit the rate of reclamation to a small fraction of each container's footprint (we chose 2\% each second) in order to avoid performance oscillations.
Indeed, if a container declares itself as green or yellow, this must mean that its WS fits into its current physical memory allocation.

This mechanism tends to adapt the amount of memory to what is required for each container to be satisfied.
Memory reclaimed from one container can be used by other containers to improve their own performance and satisfaction.

Other, more detailed, technical decisions are documented by comments in the code itself.
We implemented as a modification of Linux code to invoke the MemOpLight algorithm once per second when there is memory pressure.
If MemOpLight fails to reclaim memory, the \texttt{soft} limit mechanism activates and so the PFRA.
Our modifications amount to $\approx 400$ lines of code \cite{thesis_linux}.

\subsection{Probes}
\label{subsec_probe}
The probe can be seen as a MAPE loop which takes places in user space while our modifications to the Linux kernel correspond to another loop which executes in kernel space \cite{kephart_vision_2003}.
In more detail, the probe does three different things:
\begin{enumerate}
	\item It collects information about the application.
	\item It compares this to a SLO, which was provided by container's owner at its startup, to evaluate its performance state.
	\item Finally, it communicates the application's performance state as the green, yellow or red colour to the Linux kernel.
\end{enumerate}
For a web server, a probe might simply read the response time from the server's log and compare it to the SLO.
If the response time is higher, then the web server performance is declared to be RED, otherwise GREEN.
Another simple example is a streaming application: in this case using all three colours is appropriate.
If streaming application sustains the SLO frame rate with high video quality, its state of performance is GREEN.
If it has to degrade video quality to sustain the SLO, its declares its state as YELLOW.
Otherwise, \textit{i.e.} if it cannot respect its SLO, its performance is declared RED.
There is a trade-off between precision (which is important in cloud environment where clients are charged for the memory they use) and the overhead of communicating the colour frequently \cite{amazon_pricing_nodate, google_vm_2020}.